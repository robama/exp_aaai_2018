import csv
TIMEOUT = 600

IN_FILE = '/home/roberto/exp_aaai_2018/benchmarks/results.csv'
reader = csv.reader(open(IN_FILE), delimiter = '|')
results = {}
for row in reader:
  inst = row[0]
  length = int(row[1])
  solver = row[2].upper()
  info = row[4]
  if inst not in results.keys():
    results[inst] = {}
  if solver not in results[inst].keys():
    results[inst][solver] = {}
  if info in ['sat','uns']:
    results[inst][solver][length] = float(row[3])
  elif info == 'out':
    results[inst][solver][length] = 'T/O'
  else:
    results[inst][solver][length] = 'N/A'

tab_len = {}
tab_ben = {}
for inst, item in results.items():
  for solver, times in sorted(item.items()):
    if solver not in tab_len.keys():
      tab_len[solver] = {}
      tab_ben[solver] = {}
    if inst not in tab_ben[solver]:
      tab_ben[solver][inst] = 0.0
    for length, time in sorted(times.items()):
      l = int(length)
      if l not in tab_len[solver].keys():
        tab_len[solver][l] = 0.0
      if time in ['T/O', 'N/A']:
        tab_len[solver][l] += TIMEOUT
        tab_ben[solver][inst] += TIMEOUT
      else:
        tab_len[solver][l] += float(time)
        tab_ben[solver][inst] += float(time)
    
OUT_FILE = '/home/roberto/exp_aaai_2018/benchmarks/table1.csv'
writer = csv.writer(open(OUT_FILE, 'w'), delimiter = ',')
writer.writerow([
  250, 500, 1000, 5000, 10000, 'anbn', 'chunk', 'leven', 'hamm', 'sql', 'strep'
])
for solver, item in sorted(tab_len.items()):
  row = [solver]
  print solver, 
  for _, time in sorted(item.items()):
    t = round(time / 6, 2)
    row += [t]
    print t,
  it = tab_ben[solver]
  print '---',
  for _, time in sorted(it.items()):
    t = round(time / 5, 2)
    row += [t] 
    print t,
  writer.writerow(row)
  print

IN_FILE = '/home/roberto/exp_aaai_2018/benchmarks/results_sql.csv'
reader = csv.reader(open(IN_FILE), delimiter = '|')
tab_sql = {}
for row in reader:
  inst = row[0]
  if inst[0] == 's':
    sat = 'sat'
  else:
    sat = 'uns'
  length = int(inst[4 : inst.rfind('_')])
  solver = row[1].upper()
  if row[3] != 'unk':
    time = float(row[2])
  else:
    time = TIMEOUT
  if solver not in tab_sql.keys():
    tab_sql[solver] = {}
  if sat not in tab_sql[solver].keys():
    tab_sql[solver][sat] = {}
  if length not in tab_sql[solver][sat].keys():
    tab_sql[solver][sat][length] = [0.0, 0.0]
  if time < TIMEOUT:
    tab_sql[solver][sat][length][0] += 1
  tab_sql[solver][sat][length][1] += time

print '=========='

OUT_FILE_AST = '/home/roberto/exp_aaai_2018/benchmarks/table2_ast.csv'
OUT_FILE_PSI = '/home/roberto/exp_aaai_2018/benchmarks/table2_psi.csv'
writer_ast = csv.writer(open(OUT_FILE_AST, 'w'), delimiter = '&')
writer_ast.writerow([
  250, 500, 1000, 5000, 10000, 
  250, 500, 1000, 5000, 10000,
  250, 500, 1000, 5000, 10000
])
writer_psi = csv.writer(open(OUT_FILE_PSI, 'w'), delimiter = '&')
writer_psi.writerow([
  250, 500, 1000, 5000, 10000, 
  250, 500, 1000, 5000, 10000,
  250, 500, 1000, 5000, 10000
])
for solver, item in sorted(tab_sql.items()):
  row_ast = [solver]
  row_psi = [solver]
  print solver,
  tot_ast = {}
  tot_psi = {}
  for sat, it in sorted(item.items()):
    for length, solv_time in sorted(it.items()):
      p = round(solv_time[0] * 5,  2)
      t = round(solv_time[1] / 20, 2)
      if sat == 'sat':
        tot_ast[length] = t
        tot_psi[length] = p
      else:
        tot_ast[length] += t
        tot_psi[length] += p
      row_ast += [t]
      row_psi += [p]
      print t,
  tt = [round(v / 2, 2) for (k, v) in sorted(tot_ast.items())]
  pp = [round(v / 2, 2) for (k, v) in sorted(tot_psi.items())]
  print tt
  row_ast += tt
  row_psi += pp
  writer_ast.writerow(row_ast)
  writer_psi.writerow(row_psi)
  
