from random import randint

PATH = '/home/roberto/exp_aaai_2018/benchmarks/sql/data'

for N in [250, 500, 1000, 5000, 10000]:
  print 'Generating Satisfiable Problems...'
  for M in range(0, 20):
    print 'Length', N, ' --- SAT Instance', M + 1
    SQL = ''
    n = N
    l = randint(0, n - 3)
    if l:
      SQL += reduce(
        lambda x, y: x + y, [chr(randint(33, 90)) for _ in range(0, l)]
      ).replace('"', ' ')
    pref = SQL
    n -= l
    l = randint(1, int((n - 1) / 2))
    expr = reduce(
      lambda x, y: x + y, [chr(randint(35, 90)) for _ in range(0, l)]
    )
    SQL += expr
    n -= l * 2
    l = randint(0, n - 1)
    b1 = l
    SQL += ' ' * l + '='
    n -= l + 1
    l = randint(0, n)
    b2 = l
    SQL += ' ' * l
    SQL += expr
    n -= l
    if n:
      suff = reduce(
        lambda x, y: x + y, [chr(randint(33, 90)) for _ in range(0, n)]
      ).replace('"', ' ')
      SQL += suff
    else:
      suff = ''
    while '??' in SQL:
      SQL = SQL.replace('??', '!?')
    pref = SQL[: len(pref)]
    suff = SQL[: -len(suff)]
    expr = SQL[len(pref) : -len(suff)]
    assert N == len(SQL) and '??' not in  SQL and '??' not in pref \
                         and '??' not in suff and '??' not in expr
    SQL = 'string SQL = "' + SQL + '";\n'
    with open(PATH + '/sat_' + str(N) + '_' + str(M + 1), 'w') as f:
      f.write(SQL)
      f.write('// int_var[0] = ' + str(b1) + '\n')
      f.write('// int_var[1] = ' + str(b2) + '\n')
      f.write('// int_var[2] = ' + str(len(expr)) + '\n')
      f.write('// string_var[0] = "' + pref + '"\n')
      f.write('// string_var[1] = "' + suff + '"\n')
      f.write('// string_var[2] = "' + expr + '"\n')

    loop = True
    while loop:
      print 'Length', N, ' --- UNS Instance', M + 1
      SQL = reduce(
        lambda x, y: x + y, [chr(randint(35, 90)) for _ in range(0, N)]
      )
      assert N == len(SQL)
      loop = False
      for i in range(1, N - 1):
        prev = SQL[i - 1]
        next = SQL[i + 1]
        if SQL[i] == '=' and prev == next:
          loop = True
          break
      if not loop:
        a = int(N / 55)
        b = int(N / 100)
        indexes = [randint(0, N) for _ in range(a - b, a + b)] 
        for i in indexes:
          if i > 1 and i < N - 1 and SQL[i - 1] != '=' and SQL[i + 1] != '=':
            SQL = SQL[ :i] + ' ' + SQL[i + 1: ]
    while '??' in SQL:
      SQL = SQL.replace('??', '!?')
    pref = SQL[: len(pref)]
    suff = SQL[: -len(suff)]
    expr = SQL[len(pref) : -len(suff)]
    assert N == len(SQL) and '??' not in  SQL and '??' not in pref \
                         and '??' not in suff and '??' not in expr
    SQL = 'string SQL = "' + SQL + '";\n'
    with open(PATH + '/uns_' + str(N) + '_' + str(M + 1), 'w') as f:
      f.write(SQL)
