#include <gecode/string.hh>
#include <gecode/string/int.hh>
#include <gecode/driver.hh>

using namespace Gecode;
using namespace String;

class StringOptions : public Options {

public:

	int N;

	StringOptions(const char* s, int n): Options(s), N(n) { this->c_d(1); }

};

class Benchmark : public Script {

	IntVarArray int_vars;
	BoolVarArray bool_vars;
	StringVarArray str_vars;

public:

	static string s;

  Benchmark(bool share, Benchmark& s): Script(share, s) {
    int_vars.update (*this, share, s.int_vars);
    str_vars.update (*this, share, s.str_vars);
    bool_vars.update(*this, share, s.bool_vars);
  }
  virtual Space* copy(bool share) {
    return new Benchmark(share, *this);
  }

  Benchmark(const StringOptions& so): Script(so) {

  	//  var_3 ++ var_4 IN ("a" ++ "b" | "b" ++ "a")
  	//  (|var_4| + -1) <= 0 AND 0 <= (|var_4| + -1) AND (|var_3| + -1) <= 0
  	//  AND 0 <= (|var_3| + -1) AND |var_5| <= 0 AND 0 <= |var_5| AND
  	//  ((|var_6| + -1 * |var_2|) + 1) <= 0 AND 0 <= ((|var_6| + -1 * |var_2|) + 1)
  	//  AND (|var_1| + -1) <= 0 AND 0 <= (|var_1| + -1) AND 0 <= (|var_2| + -2)

  	// Variables.
  	NSBlocks v = NSBlocks(1, NSBlock(NSIntSet('a', 'b'), 0, so.N));
  	StringVar var_1(*this, 0, so.N);
  	StringVar var_2(*this, 0, so.N);
    StringVar var_3(*this, 0, so.N);
    StringVar var_4(*this, 0, so.N);
  	StringVar var_5(*this, 0, so.N);
  	StringVar var_6(*this, 0, so.N);
  	StringVar var_34(*this, v);
  	StringVarArgs sva;
  	sva << var_1 << var_2 << var_3 << var_4 << var_5 << var_6 << var_34;
  	str_vars = StringVarArray(*this, sva);
  	IntVar l_1(*this, 0, so.N);
  	IntVar l_2(*this, 0, so.N);
  	IntVar l_3(*this, 0, so.N);
  	IntVar l_4(*this, 0, so.N);
  	IntVar l_5(*this, 0, so.N);
  	IntVar l_6(*this, 0, so.N);
  	IntVarArgs iva;
  	iva << l_1 << l_2 << l_3 << l_4 << l_5 << l_6;
  	int_vars = IntVarArray(*this, iva);
  	BoolVar b1(*this, 0, 1);
  	BoolVar b2(*this, 0, 1);
  	BoolVarArgs bva;
  	bva << b1 << b2;
  	bool_vars = BoolVarArray(*this, bva);

  	// Constraints.
  	length(*this, var_1, l_1);
  	length(*this, var_2, l_2);
  	length(*this, var_3, l_3);
  	length(*this, var_4, l_4);
  	length(*this, var_5, l_5);
  	length(*this, var_6, l_6);
  	rel(*this, l_4 + (-1) <= 0);
		rel(*this, 0 <= l_4 + (-1));
		rel(*this, l_3 + (-1) <= 0);
		rel(*this, 0 <= l_3 + (-1));
		rel(*this, l_5 <= 0);
		rel(*this, 0 <= l_5);
		rel(*this, (l_6 + (-1) * l_2) + 1 <= 0);
		rel(*this, (l_6 + (-1) * l_2) + 1 >= 0);
		rel(*this, l_1 + -1 <= 0);
		rel(*this, l_1 + -1 >= 0);
		rel(*this, 0 <= l_2 + -2);

  	rel(*this, var_3, var_4, STRT_CAT, var_34);
    rel(*this, var_34, STRT_EQ, StringVar(*this, "ab"), Reify(b1));
    rel(*this, var_34, STRT_EQ, StringVar(*this, "ba"), Reify(b2));
    rel(*this, b1 + b2 >= 1);

  	// Branching.
    sizemin_llul(*this, str_vars);
  }

  virtual void
	print(std::ostream& os) const {
  	s = str_vars[str_vars.size() - 1].val();
  	for (int i = 0; i < bool_vars.size(); ++i)
			if (bool_vars[i].assigned())
				os << "bool_var[" << i << "] = " << bool_vars[i].val() << "\n";
			else
				os << "bool_var[" << i << "] = " << bool_vars[i] << "\n";
  	for (int i = 0; i < int_vars.size(); ++i)
  		if (int_vars[i].assigned())
  	    os << "int_var[" << i << "] = " << int_vars[i].val() << "\n";
  		else
  			os << "int_var[" << i << "] = " << int_vars[i] << "\n";
    for (int i = 0; i < str_vars.size(); ++i)
    	if (str_vars[i].assigned())
  	    os << "string_var[" << i << "] = \"" << str_vars[i].val() << "\"\n";
    	else
    		os << "string_var[" << i << "] = \"" << str_vars[i] << "\"\n";
  	os << "----------\n";
  }

};

string Benchmark::s = "";

int main(int argc, char* argv[]) {
	StringOptions opt("*** levenshtein ***", atoi(argv[1]));
	if (argc > 1) {
		string s(argv[2]);
		if (s == "--cover") {
			DashedString::_MAX_COVER = DashedString::_MAX_STR_LENGTH;
			DashedString::_MAX_SWEEP = -1;
		}
		else if (s == "--sweep") {
			DashedString::_MAX_COVER = -1;
			DashedString::_MAX_SWEEP = DashedString::_MAX_STR_LENGTH;
		}
	}
	opt.solutions(1);
	Script::run<Benchmark, DFS, StringOptions>(opt);
	assert (Benchmark::s == "ab");
 	return 0;
}
