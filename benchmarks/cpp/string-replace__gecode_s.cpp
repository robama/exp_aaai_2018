/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), [('str.++',), ('var_5',), 'z', 'b', ('var_6',)], [('re.++',), [('re.*',), [('str.to.re',), 'z']], [('re.union',), [('str.to.re',), ''], [('re.++',), [('re.union',), [('str.to.re',), 'b'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]], [('re.*',), [('re.union',), [('str.to.re',), 'b'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]]]]]]]]
[('assert',), [('str.in.re',), [('str.++',), ('var_5',), 'z', 'b', ('var_6',)], [('re.++',), [('re.*',), [('re.++',), [('re.union',), [('str.to.re',), 'z'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]], [('re.++',), [('re.*',), [('re.union',), [('str.to.re',), 'b'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]]], [('str.to.re',), 'z']]]], [('re.++',), [('re.union',), [('str.to.re',), 'z'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]], [('re.*',), [('re.union',), [('str.to.re',), 'b'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]]]]]]]
[('assert',), [('str.in.re',), [('str.++',), ('var_5',), 'z', 'b', ('var_6',)], [('re.++',), [('re.*',), [('re.union',), [('str.to.re',), 'z'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]]], [('re.union',), [('str.to.re',), ''], [('re.++',), [('str.to.re',), 'b'], [('re.*',), [('re.union',), [('str.to.re',), 'b'], [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'c']]]]]]]]]
[('assert',), [('str.in.re',), [('str.++',), 'b', ('var_6',)], [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('str.in.re',), ('var_5',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('check-sat',)]
*/

#include "string-model.hh"
#include <stdlib.h>     /* atoi */

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {    
	//DFAs:
	REG r_0 = (*(REG(char2val('z')))) + (REG() | (((REG(char2val('b'))) | ((REG(char2val('a'))) | (REG(char2val('c'))))) + (*((REG(char2val('b'))) | ((REG(char2val('a'))) | (REG(char2val('c'))))))));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = (*(((REG(char2val('z'))) | ((REG(char2val('a'))) | (REG(char2val('c'))))) + ((*((REG(char2val('b'))) | ((REG(char2val('a'))) | (REG(char2val('c')))))) + (REG(char2val('z')))))) + (((REG(char2val('z'))) | ((REG(char2val('a'))) | (REG(char2val('c'))))) + (*((REG(char2val('b'))) | ((REG(char2val('a'))) | (REG(char2val('c')))))));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	REG r_2 = (*((REG(char2val('z'))) | ((REG(char2val('a'))) | (REG(char2val('c')))))) + (REG() | ((REG(char2val('b'))) + (*((REG(char2val('b'))) | ((REG(char2val('a'))) | (REG(char2val('c'))))))));
	DFA d_2(r_2);
	BitSet a_2 = alphabetof(d_2);
	REG r_3 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_3(r_3);
	BitSet a_3 = alphabetof(d_3);
	REG r_4 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_4(r_4);
	BitSet a_4 = alphabetof(d_4);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar temp_9(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_3),opt.block());
	_x << temp_9;
	StringVar temp_8(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_2),opt.block());
	_x << temp_8;
	StringVar temp_5(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_1),opt.block());
	_x << temp_5;
	StringVar temp_4(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_4;
	StringVar temp_7(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_7;
	StringVar temp_6(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_6;
	StringVar temp_1(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_1;
	StringVar temp_0(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_0;
	StringVar temp_3(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_3;
	StringVar temp_2(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_0),opt.block());
	_x << temp_2;
	StringVar var_6(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_6;
	StringVar var_5(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_0),opt.block());
	_x << var_5;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	concat(*this,var_5,str2IntArgs("z"),temp_0);
	concat(*this,str2IntArgs("b"),var_6,temp_1);
	concat(*this,temp_0,temp_1,temp_2);
	extensional(*this,temp_2,d_0);
	concat(*this,var_5,str2IntArgs("z"),temp_3);
	concat(*this,str2IntArgs("b"),var_6,temp_4);
	concat(*this,temp_3,temp_4,temp_5);
	extensional(*this,temp_5,d_1);
	concat(*this,var_5,str2IntArgs("z"),temp_6);
	concat(*this,str2IntArgs("b"),var_6,temp_7);
	concat(*this,temp_6,temp_7,temp_8);
	extensional(*this,temp_8,d_2);
	concat(*this,str2IntArgs("b"),var_6,temp_9);
	extensional(*this,temp_9,d_3);
	//extensional(*this,var_5,d_4);

    
        post_brancher(x,opt);
      }
  
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}

    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
  
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };

    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
	StringOptions opt("Benchmark::StringReplace/norn-benchmark-136", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);

      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    
