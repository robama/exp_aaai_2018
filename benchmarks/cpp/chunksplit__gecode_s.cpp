/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), [('str.++',), ('var_5',), ('var_6',), ('var_7',), 'z', ('var_5',)], [('re.*',), [('re.union',), [('str.to.re',), 'z'], [('re.union',), [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'b']], [('str.to.re',), 'c']]]]]]
[('assert',), [('str.in.re',), ('var_5',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('str.in.re',), [('str.++',), ('var_5',), ('var_6',), ('var_7',)], [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('and',), [('and',), [('and',), [('and',), [('and',), [('<=',), [('+',), [('str.len',), ('var_6',)], [('-',), 8]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_6',)], [('-',), 8]]]], [('and',), [('<=',), [('+',), [('+',), [('+',), [('str.len',), ('var_7',)], [('str.len',), ('var_5',)]], [('*',), [('-',), 1], [('str.len',), ('var_2',)]]], 8], 0], [('<=',), 0, [('+',), [('+',), [('+',), [('str.len',), ('var_7',)], [('str.len',), ('var_5',)]], [('*',), [('-',), 1], [('str.len',), ('var_2',)]]], 8]]]], [('and',), [('<=',), [('+',), [('+',), [('str.len',), ('var_3',)], [('*',), [('-',), 1], [('str.len',), ('var_5',)]]], [('-',), 8]], 0], [('<=',), 0, [('+',), [('+',), [('str.len',), ('var_3',)], [('*',), [('-',), 1], [('str.len',), ('var_5',)]]], [('-',), 8]]]]], [('<=',), 0, [('+',), [('+',), [('*',), [('-',), 1], [('str.len',), ('var_5',)]], [('str.len',), ('var_2',)]], [('-',), 8]]]], [('<=',), 0, [('str.len',), ('var_5',)]]]]
[('check-sat',)]
*/

#include "string-model.hh"
#include <stdlib.h>     /* atoi */

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {    
	//DFAs:
	REG r_0 = *((REG(char2val('z'))) | (((REG(char2val('a'))) | (REG(char2val('b')))) | (REG(char2val('c')))));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	REG r_2 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_2(r_2);
	BitSet a_2 = alphabetof(d_2);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar temp_1(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_1;
	StringVar temp_4(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_4;
	StringVar temp_0(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_0;
	StringVar temp_2(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_2;
	StringVar temp_5(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_2),opt.block());
	_x << temp_5;
	StringVar temp_3(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_0),opt.block());
	_x << temp_3;
	StringVar var_7(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_7;
	StringVar var_6(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_6;
	StringVar var_5(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_1),opt.block());
	_x << var_5;
	StringVar var_3(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_3;
	StringVar var_2(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_2;
	IntVar len_var_3(*this,opt.minlength(),opt.length());
	_l << len_var_3;
	IntVar len_var_2(*this,opt.minlength(),opt.length());
	_l << len_var_2;
	IntVar len_var_7(*this,opt.minlength(),opt.length());
	_l << len_var_7;
	IntVar len_var_6(*this,opt.minlength(),opt.length());
	_l << len_var_6;
	IntVar len_var_5(*this,opt.minlength(),opt.length());
	_l << len_var_5;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	concat(*this,var_5,var_6,temp_0);
	concat(*this,var_7,str2IntArgs("z"),temp_1);
	concat(*this,temp_0,temp_1,temp_2);
	concat(*this,temp_2,var_5,temp_3);
	extensional(*this,temp_3,d_0);
	extensional(*this,var_5,d_1);
	concat(*this,var_5,var_6,temp_4);
	concat(*this,temp_4,var_7,temp_5);
	extensional(*this,temp_5,d_2);
	length(*this,var_6,len_var_6);
	rel(*this, (len_var_6) + (-8) <= 0);
	length(*this,var_6,len_var_6);
	rel(*this, 0 <= (len_var_6) + (-8));
	length(*this,var_7,len_var_7);
	length(*this,var_5,len_var_5);
	length(*this,var_2,len_var_2);
	rel(*this, (((len_var_7) + (len_var_5)) + ((-1) * (len_var_2))) + (8) <= 0);
	length(*this,var_7,len_var_7);
	length(*this,var_5,len_var_5);
	length(*this,var_2,len_var_2);
	rel(*this, 0 <= (((len_var_7) + (len_var_5)) + ((-1) * (len_var_2))) + (8));
	length(*this,var_3,len_var_3);
	length(*this,var_5,len_var_5);
	rel(*this, ((len_var_3) + ((-1) * (len_var_5))) + (-8) <= 0);
	length(*this,var_3,len_var_3);
	length(*this,var_5,len_var_5);
	rel(*this, 0 <= ((len_var_3) + ((-1) * (len_var_5))) + (-8));
	length(*this,var_5,len_var_5);
	length(*this,var_2,len_var_2);
	rel(*this, 0 <= (((-1) * (len_var_5)) + (len_var_2)) + (-8));
	length(*this,var_5,len_var_5);
	rel(*this, 0 <= len_var_5);

    
        post_brancher(x,opt);
      }
  
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}

    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
  
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };

    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
	StringOptions opt("Benchmark::ChunkSplit/norn-benchmark-25", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);

      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    
