
class Benchmark : public Script {

	IntVarArray int_vars;
	StringVarArray str_vars;

public:

  Benchmark(bool share, Benchmark& s): Script(share, s) {
    int_vars.update(*this, share, s.int_vars);
    str_vars.update(*this, share, s.str_vars);
  }
  virtual Space* copy(bool share) {
    return new Benchmark(share, *this);
  }

  Benchmark(const Options& o): Script(o) {
    int N = SQL.size();
//  	constraint str_len(expr) > 0;
//  	constraint blank1 = str_pow(" ", n);
//  	constraint blank2 = str_pow(" ", m);
//  	constraint sql =
//  	  pref ++ expr ++ blank1 ++ "=" ++ blank2 ++ expr ++ suff

  	// Variables.
    StringVar pref(*this, 0, N);
    StringVar suff(*this, 0, N);
    StringVar expr(*this, 0, N);
    StringVar blank1(*this, 0, N);
    StringVar blank2(*this, 0, N);
    StringVar pref_expr(*this, 0, N);
	  StringVar blank_expr(*this, 0, N);
	  StringVar lhs(*this, 0, N);
	  StringVar rhs(*this, 0, N);
	  StringVar eq (*this, 0, N);
    StringVarArgs sva;
    sva << blank1 << blank2 << pref << suff << expr
    		<< pref_expr << blank_expr << lhs << rhs << eq;
  	str_vars = StringVarArray(*this, sva);

  	IntVar n(*this, 0, N);
  	IntVar m(*this, 0, N);
  	IntVar l(*this, 0, N);
  	IntVarArgs iva;
  	iva << n << m << l;
  	int_vars = IntVarArray(*this, iva);

  	// Constraints.
  	length(*this, expr, l);
  	length(*this, blank1, n);
  	length(*this, blank2, m);
  	rel(*this, l > 0);
  	NSBlocks v({NSBlock(NSIntSet(' '), 0, N)});
  	
  	// FIXME: New constraint.
  	//NSBlocks w({NSBlock(NSIntSet('!', 'Z'), 0, N)});
    //rel(*this, expr, STRT_EQ, StringVar(*this, w)); //STRT_DOM, w);
  	
  	//FIXME: Fix STRT_DOM.
  	rel(*this, blank1, STRT_EQ, StringVar(*this, v)); //STRT_DOM, v);
  	rel(*this, blank2, STRT_EQ, StringVar(*this, v)); //STRT_DOM, v);
    rel(*this, pref, expr, STRT_CAT, pref_expr);
    rel(*this, pref_expr, blank1, STRT_CAT, lhs);
    rel(*this, lhs, StringVar(*this, "="), STRT_CAT, eq);
    rel(*this, blank2, expr, STRT_CAT, blank_expr);
    rel(*this, blank_expr, suff, STRT_CAT, rhs);
    rel(*this, eq, rhs, STRT_CAT, StringVar(*this, SQL));

  	// Branching.
    sizemin_llul(*this, str_vars);
  }

  virtual void
	print(std::ostream& os) const {
  	for (int i = 0; i < int_vars.size(); ++i)
  		if (int_vars[i].assigned())
  	    os << "int_var[" << i << "] = " << int_vars[i].val() << "\n";
  		else
  			os << "int_var[" << i << "] = " << int_vars[i] << "\n";
    for (int i = 0; i < 5; ++i)
    	if (str_vars[i].assigned())
  	    os << "string_var[" << i << "] = \"" << str_vars[i].val() << "\"\n";
    	else
    		os << "string_var[" << i << "] = \"" << str_vars[i] << "\"\n";
  	os << "----------\n";
  }

};


int main(int, char* argv[]) {
	Options opt("*** sql ***");
        opt.c_d(1);
	string s(argv[1]);
	if (s == "--cover") {
		DashedString::_MAX_COVER = DashedString::_MAX_STR_LENGTH;
		DashedString::_MAX_SWEEP = -1;
	}
	else if (s == "--sweep") {
		DashedString::_MAX_COVER = -1;
		DashedString::_MAX_SWEEP = DashedString::_MAX_STR_LENGTH;
	}
	opt.solutions(1);
	Script::run<Benchmark, DFS, Options>(opt);
 	return 0;
}
