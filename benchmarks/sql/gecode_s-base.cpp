class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {    
	char* lit = " ";
	size_t len_lit = std::strlen(lit);
	
	//DFAs:
	REG r = *REG(char2val(' '));
	DFA d(r);
	//BitSet alpha = alphabetof(d);
	
	
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar pref(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << pref;
	StringVar suff(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << suff;
	StringVar expr(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << expr;
	StringVar sql(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << sql;
	// temp vars for replacing the str_pow expressions
	StringVar pow_1(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << pow_1;
	StringVar pow_2(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << pow_2;
	
	StringVar cat_1(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_1;
	StringVar cat_2(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_2;
	StringVar cat_3(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_3;
	StringVar cat_4(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_4;
	StringVar cat_5(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_5;
	
	IntVar n(*this,opt.minlength(),opt.length());
	_l << n;
	IntVar m(*this,opt.minlength(),opt.length());
	_l << m;
	// temp vars for str_len constraints
	IntVar len_expr(*this,opt.minlength(),opt.length());
	_l << len_expr;
	
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	
	//Constraints:
	// str_pow
	//extensional(*this,pow_1,d);
	alphabet(*this, pow_1, char2val(' '), char2val(' '));
	length(*this,pow_1,n);
	
	// str_pow
	//extensional(*this,pow_2,d);
	alphabet(*this, pow_2, char2val(' '), char2val(' '));
	length(*this,pow_2,m);
	
	// concat
	concat(*this, pref, expr, cat_1);
	concat(*this, pow_1, str2IntArgs("="), cat_2);
	concat(*this, cat_1, cat_2, cat_3);
	concat(*this, pow_2, expr, cat_4);
	concat(*this, cat_4, suff, cat_5);
	concat(*this, cat_3, cat_5, sql);
  equal(*this, sql, str2IntArgs(SQL));
	
	// sql length
	length(*this,expr,len_expr);
	rel(*this, 1 <= len_expr);

  // FIXME: New constraint.
  //alphabet(*this, expr, char2val('!'), char2val('Z'));

  post_brancher(x,opt);
	
  }

	Benchmark(bool share, Benchmark& s)
    : StringModel(share,s) {}

	virtual Space* copy(bool share) {
		return new Benchmark(share,*this);
	}
};

int main(int argc, char* argv[]) {
  Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
  StringOptions opt("SQL Injection", SQL.size());
  opt.solutions(1);
  opt.min(32);
  StringModel::standardOptions(opt);
  opt.parse(argc,argv);
  Script::run<Benchmark,DFS,StringOptions>(opt);
	return 0;
}
